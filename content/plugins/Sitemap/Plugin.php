<?php

namespace content\plugins\Sitemap;

use JsonDb\JsonDb\Db;
use system\library\Route;

/**
 * @package 网站地图
 * @description 提供 sitemap.xml 网站地图
 * @author 易航
 * @version 1.0
 * @link http://blog.bri6.cn
 */
class Plugin
{
	public static function route()
	{
		// 注册统计路由
		Route::rule('/sitemap.xml', function ($param) {
			self::sitemap();
			exit;
		});
	}

	private static function sitemap()
	{
		header('Content-type: text/xml');
		// 网站根目录
		$base_url = request('scheme') . '://' . DOMAIN . '/';

		// 网站地图的版本
		// $sitemap_version = '1.0';

		// 定义需要包含在网站地图中的页面
		$theme_files = scan_dir(THEME_ROOT)->files(['php']);
		$page_list = [];
		foreach ($theme_files as $key => $file) {
			$file = THEME_ROOT . DIR_SEP . $file;
			$themeAnnotation = get_file_annotate($file);
			if ($themeAnnotation) {
				$themeInfo = parse_annotate($themeAnnotation);
				if (!empty($themeInfo[1]) && !empty($themeInfo['package']) && $themeInfo['package'] == 'custom') {
					$page_list[] = str_replace('.php', '.html', basename($file));
				}
			}
		}

		// 链接
		$site_list = Db::name('site')->where('status', 1)->select();
		foreach ($site_list as $key => $value) {
			$site_list[$key]['lastmod'] = str_replace(' ', 'T', $value['update_time']) . 'Z';
		}

		// 生成网站地图 XML
		echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
		echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		echo '<url>';
		echo '<loc>' . $base_url . '</loc>';
		echo '<lastmod>' . date('Y-m-d') . 'T' . date('H:i:s') . 'Z</lastmod>';
		echo '<changefreq>weekly</changefreq>';
		echo '<priority>1.0</priority>';
		echo '</url>';

		// 生成页面链接
		foreach ($page_list as $page) {
			echo '<url>';
			echo '<loc>' . $base_url . $page . '</loc>';
			echo '<lastmod>' . date('Y-m-d') . 'T' . date('H:i:s') . 'Z</lastmod>';
			echo '<changefreq>weekly</changefreq>';
			echo '<priority>0.8</priority>';
			echo '</url>';
		}

		// 生成链接
		foreach ($site_list as $site) {
			echo '<url>';
			echo '<loc>' . $base_url . $site['id'] . '.html</loc>';
			echo '<lastmod>' . $site['lastmod'] . '</lastmod>';
			echo '<changefreq>weekly</changefreq>';
			echo '<priority>0.5</priority>';
			echo '</url>';
		}

		echo '</urlset>';
	}
}
