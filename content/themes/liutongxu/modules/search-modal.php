<div class="modal fade search-modal" id="search-modal">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div id="search" class="s-search mx-auto my-4">
                    <div id="search-list" class="hide-type-list">
                        <div class="s-type">
                            <span></span>
                            <div class="s-type-list">
                                <label for="m_type-baidu" data-id="group-a">常用</label><label for="m_type-baidu1" data-id="group-b">搜索</label><label for="m_type-br" data-id="group-c">工具</label><label for="m_type-zhihu" data-id="group-d">社区</label><label for="m_type-taobao1" data-id="group-e">生活</label><label for="m_type-zhaopin" data-id="group-f">求职</label>
                            </div>
                        </div>
                        <div class="search-group group-a"><span class="type-text text-muted">常用</span>
                            <ul class="search-type">
                                <li><input checked="checked" hidden="" type="radio" name="type2" id="m_type-baidu" value="https://www.baidu.com/s?wd=" data-placeholder="百度一下"><label for="m_type-baidu"><span class="text-muted">百度</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-google" value="https://www.google.com/search?q=" data-placeholder="谷歌两下"><label for="m_type-google"><span class="text-muted">谷歌</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-zhannei" value="https://www.423down.com/?s=" data-placeholder="软件搜索"><label for="m_type-zhannei"><span class="text-muted">软件</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-taobao" value="https://s.taobao.com/search?q=" data-placeholder="淘宝"><label for="m_type-taobao"><span class="text-muted">淘宝</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-bing" value="https://soupian.one/search?key=" data-placeholder="影视资源聚合搜索"><label for="m_type-bing"><span class="text-muted">影视</span></label></li>
                            </ul>
                        </div>
                        <div class="search-group group-b"><span class="type-text text-muted">搜索</span>
                            <ul class="search-type">
                                <li><input hidden="" type="radio" name="type2" id="m_type-baidu1" value="https://www.baidu.com/s?wd=" data-placeholder="百度一下"><label for="m_type-baidu1"><span class="text-muted">百度</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-google1" value="https://www.google.com/search?q=" data-placeholder="谷歌两下"><label for="m_type-google1"><span class="text-muted">谷歌</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-360" value="https://www.so.com/s?q=" data-placeholder="360好搜"><label for="m_type-360"><span class="text-muted">360</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-sogo" value="https://www.sogou.com/web?query=" data-placeholder="搜狗搜索"><label for="m_type-sogo"><span class="text-muted">搜狗</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-bing1" value="https://cn.bing.com/search?q=" data-placeholder="必应搜索"><label for="m_type-bing1"><span class="text-muted">必应</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-sm" value="https://yz.m.sm.cn/s?q=" data-placeholder="神马搜索"><label for="m_type-sm"><span class="text-muted">神马</span></label></li>
                            </ul>
                        </div>
                        <div class="search-group group-c"><span class="type-text text-muted">工具</span>
                            <ul class="search-type">
                                <li><input hidden="" type="radio" name="type2" id="m_type-br" value="https://rank.chinaz.com/all/" data-placeholder="请输入网址(不带https://)"><label for="m_type-br"><span class="text-muted">权重查询</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-links" value="https://link.chinaz.com/" data-placeholder="请输入网址(不带https://)"><label for="m_type-links"><span class="text-muted">友链检测</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-icp" value="https://icp.aizhan.com/" data-placeholder="请输入网址(不带https://)"><label for="m_type-icp"><span class="text-muted">备案查询</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-ping" value="https://ping.chinaz.com/" data-placeholder="请输入网址(不带https://)"><label for="m_type-ping"><span class="text-muted">PING检测</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-404" value="https://tool.chinaz.com/Links/?DAddress=" data-placeholder="请输入网址(不带https://)"><label for="m_type-404"><span class="text-muted">死链检测</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-ciku" value="https://www.ciku5.com/s?wd=" data-placeholder="请输入关键词"><label for="m_type-ciku"><span class="text-muted">关键词挖掘</span></label></li>
                            </ul>
                        </div>
                        <div class="search-group group-d"><span class="type-text text-muted">社区</span>
                            <ul class="search-type">
                                <li><input hidden="" type="radio" name="type2" id="m_type-zhihu" value="https://www.zhihu.com/search?type=content&q=" data-placeholder="知乎"><label for="m_type-zhihu"><span class="text-muted">知乎</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-wechat" value="https://weixin.sogou.com/weixin?type=2&query=" data-placeholder="微信"><label for="m_type-wechat"><span class="text-muted">微信</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-weibo" value="https://s.weibo.com/weibo/" data-placeholder="微博"><label for="m_type-weibo"><span class="text-muted">微博</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-douban" value="https://www.douban.com/search?q=" data-placeholder="豆瓣"><label for="m_type-douban"><span class="text-muted">豆瓣</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-why" value="https://ask.seowhy.com/search/?q=" data-placeholder="SEO问答社区"><label for="m_type-why"><span class="text-muted">搜外问答</span></label></li>
                            </ul>
                        </div>
                        <div class="search-group group-e"><span class="type-text text-muted">生活</span>
                            <ul class="search-type">
                                <li><input hidden="" type="radio" name="type2" id="m_type-taobao1" value="https://s.taobao.com/search?q=" data-placeholder="淘宝"><label for="m_type-taobao1"><span class="text-muted">淘宝</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-jd" value="https://search.jd.com/Search?keyword=" data-placeholder="京东"><label for="m_type-jd"><span class="text-muted">京东</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-xiachufang" value="https://www.xiachufang.com/search/?keyword=" data-placeholder="下厨房"><label for="m_type-xiachufang"><span class="text-muted">下厨房</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-xiangha" value="https://www.xiangha.com/so/?q=caipu&s=" data-placeholder="香哈菜谱"><label for="m_type-xiangha"><span class="text-muted">香哈菜谱</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-12306" value="https://www.12306.cn/?" data-placeholder="12306"><label for="m_type-12306"><span class="text-muted">12306</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-kd100" value="https://www.kuaidi100.com/?" data-placeholder="快递100"><label for="m_type-kd100"><span class="text-muted">快递100</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-qunar" value="https://www.qunar.com/?" data-placeholder="去哪儿"><label for="m_type-qunar"><span class="text-muted">去哪儿</span></label></li>
                            </ul>
                        </div>
                        <div class="search-group group-f"><span class="type-text text-muted">求职</span>
                            <ul class="search-type">
                                <li><input hidden="" type="radio" name="type2" id="m_type-zhaopin" value="https://sou.zhaopin.com/jobs/searchresult.ashx?kw=" data-placeholder="智联招聘"><label for="m_type-zhaopin"><span class="text-muted">智联招聘</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-51job" value="https://search.51job.com/?" data-placeholder="前程无忧"><label for="m_type-51job"><span class="text-muted">前程无忧</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-lagou" value="https://www.lagou.com/jobs/list_" data-placeholder="拉钩网"><label for="m_type-lagou"><span class="text-muted">拉钩网</span></label></li>
                                <li><input hidden="" type="radio" name="type2" id="m_type-liepin" value="https://www.liepin.com/zhaopin/?key=" data-placeholder="猎聘网"><label for="m_type-liepin"><span class="text-muted">猎聘网</span></label></li>
                            </ul>
                        </div>
                    </div>
                    <form action="https://nav.iowen.cn?s=" method="get" target="_blank" class="super-search-fm">
                        <input type="text" id="m_search-text" class="form-control smart-tips search-key" zhannei="" autocomplete="off" placeholder="输入关键字搜索" style="outline:0">
                        <button type="submit"><i class="iconfont icon-search"></i></button>
                    </form>
                    <div class="card search-smart-tips" style="display: none">
                        <ul></ul>
                    </div>
                </div>
                <!-- <div class="px-1 mb-3"><i class="text-xl iconfont icon-hot mr-1" style="color:#f1404b;"></i><span class="h6">热门推荐： </span></div> -->
                <!-- <div class="mb-3">
                    <li id="menu-item-332" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-332"><a href="tag/黑洞.html">黑洞</a></li>
                    <li id="menu-item-333" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-333"><a href="E5AFBCE888AAE4B8BBE9A298.html">导航主题</a></li>
                    <li id="menu-item-335" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-335"><a href="E59BBEE6A087.html">图标</a></li>
                </div> -->
            </div>
            <div style="position: absolute;bottom: -40px;width: 100%;text-align: center;"><a href="javascript:" data-dismiss="modal"><i class="iconfont icon-gary icon-2x" style="color: #fff;"></i></a>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' src='<?= $this->cdn('jqueryui/1.12.1/jquery-ui.min.js') ?>' id='jquery-ui-js'></script>
<script type='text/javascript' src='assets/js/jquery.ui.touch-punch.min.js' id='jqueryui-touch-js'></script>
<script type='text/javascript' src='assets/js//clipboard.min-5.6.2.js' id='clipboard-js'></script>
<script type='text/javascript' id='popper-js-extra'>
    /* <![CDATA[ */
    var theme = {
        "ajaxurl": "https:\/\/nav.iowen.cn\/wp-admin\/admin-ajax.php",
        "addico": "https:\/\/nav.iowen.cn\/wp-content\/themes\/onenav\/images\/add.png",
        "order": "asc",
        "formpostion": "top",
        "defaultclass": "io-grey-mode",
        "isCustomize": "1",
        "icourl": "https:\/\/api.iowen.cn\/favicon\/",
        "icopng": ".png",
        "urlformat": "1",
        "customizemax": "10",
        "newWindow": "0",
        "lazyload": "1",
        "minNav": "1",
        "loading": "1",
        "hotWords": "baidu",
        "classColumns": " col-sm-6 col-md-4 col-xl-5a col-xxl-6a ",
        "apikey": "TWpBeU1UVTNOekk1TWpVMEIvZ1M2bFVIQllUMmxsV1dZelkxQTVPVzB3UW04eldGQmxhM3BNWW14bVNtWk4="
    };
    /* ]]> */
</script>
<script type='text/javascript' src='assets/js/popper.min.js' id='popper-js'></script>
<script type='text/javascript' src='<?= $this->cdn('twitter-bootstrap/4.3.1/js/bootstrap.min.js') ?>' id='bootstrap-js'></script>
<script type='text/javascript' src='assets/js/theia-sticky-sidebar.js' id='sidebar-js'></script>
<script type='text/javascript' src='<?= $this->cdn('vanilla-lazyload/12.4.0/lazyload.min.js') ?>' id='lazyload-js'></script>
<script type='text/javascript' src='<?= $this->cdn('fancybox/3.5.7/jquery.fancybox.min.js') ?>' id='lightbox-js-js'></script>
<script type='text/javascript' src='assets/js/app.js' id='appjs-js'></script>
<script type="text/javascript">
    $(document).ready(function() {
        var siteWelcome = $('#loading');
        siteWelcome.addClass('close');
        setTimeout(function() {
            siteWelcome.remove();
        }, 600);
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            if ($('a.smooth[href="' + window.location.hash + '"]')[0]) {
                $('a.smooth[href="' + window.location.hash + '"]').click();
            } else if (window.location.hash != '') {
                $("html, body").animate({
                    scrollTop: $(window.location.hash).offset().top - 90
                }, {
                    duration: 500,
                    easing: "swing"
                });
            }
        }, 300);
        $(document).on('click', 'a.smooth', function(ev) {
            ev.preventDefault();
            if ($('#sidebar').hasClass('show') && !$(this).hasClass('change-href')) {
                $('#sidebar').modal('toggle');
            }
            if ($(this).attr("href").substr(0, 1) == "#") {
                $("html, body").animate({
                    scrollTop: $($(this).attr("href")).offset().top - 90
                }, {
                    duration: 500,
                    easing: "swing"
                });
            }
            if ($(this).hasClass('go-search-btn')) {
                $('#search-text').focus();
            }
            if (!$(this).hasClass('change-href')) {
                var menu = $("a" + $(this).attr("href"));
                menu.click();
                toTarget(menu.parent().parent(), true, true);
            }
        });
        $(document).on('click', 'a.tab-noajax', function(ev) {
            var url = $(this).data('link');
            if (url)
                $(this).parents('.d-flex.flex-fill.flex-tab').children('.btn-move.tab-move').show().attr('href', url);
            else
                $(this).parents('.d-flex.flex-fill.flex-tab').children('.btn-move.tab-move').hide();
        });
    });
</script>
<!-- 自定义代码 -->
<script>
    //夜间模式
    (function() {
        if (document.cookie.replace(/(?:(?:^|.*;\s*)night\s*\=\s*([^;]*).*$)|^.*$/, "$1") === '') {
            if (new Date().getHours() > 22 || new Date().getHours() < 6) {
                document.body.classList.remove('io-black-mode');
                document.body.classList.add('io-grey-mode');
                document.cookie = "night=1;path=/";
                console.log('夜间模式开启');
            } else {
                document.body.classList.remove('night');
                document.cookie = "night=0;path=/";
                console.log('夜间模式关闭');
            }
        } else {
            var night = document.cookie.replace(/(?:(?:^|.*;\s*)night\s*\=\s*([^;]*).*$)|^.*$/, "$1") || '0';
            if (night == '0') {
                document.body.classList.remove('night');
            } else if (night == '1') {
                document.body.classList.add('night');
            }
        }
    })();
    //夜间模式切换
    function switchNightMode() {
        var night = document.cookie.replace(/(?:(?:^|.*;\s*)night\s*\=\s*([^;]*).*$)|^.*$/, "$1") || '0';
        if (night == '0') {
            document.body.classList.remove('io-grey-mode');
            document.body.classList.add('io-black-mode');
            document.cookie = "night=1;path=/"
            console.log(' ');
            $(".switch-dark-mode").attr("data-original-title", "日间模式");
            $(".mode-ico").removeClass("icon-yejian");
            $(".mode-ico").addClass("icon-yejianms");
        } else {
            document.body.classList.remove('io-black-mode');
            document.body.classList.add('io-grey-mode');
            document.cookie = "night=0;path=/"
            console.log(' ');
            $(".switch-dark-mode").attr("data-original-title", "夜间模式");
            $(".mode-ico").removeClass("icon-yejianms");
            $(".mode-ico").addClass("icon-yejian");
        }
    }
</script> <!-- 看板 -->
<!-- <script src="live2d/autoload.js"></script> -->
<!--五颜六色特效-->
<canvas class="fireworks" style="position:fixed;left:0;top:0;z-index:99999999;pointer-events:none;"></canvas>
<script type="text/javascript" src="assets/cur/aixintexiao.js"></script>
<!-- end 自定义代码 -->
<?= $this->footer() ?>