# 易航网址引导系统

## 系统简介

`易航网址引导系统` 综合了目前网上大多数UI漂亮的引导页面 是易航原创一款极其优雅的易航网址引导页管理系统，这虽是是一款轻量级系统，但功能繁多而不沉淀。它支持多种前台主题互换和插件化开发，它不仅是一个网址引导系统，同时也是一个功能强大的网址导航系统。它具有以下显著特点和优势：

- 解决网址变更的问题： 在诸如卡盟行业和其他需要频繁更换网址的场景中，易航网址引导系统提供了一个稳定的平台。用户无需担心客户因为找不到新的网址而流失的问题。
- 多样化的主题和自定义能力： 系统内置多达26套主题，每套主题都可以进行自定义设置。这使得用户可以根据自己的品牌风格或需求，轻松地创建个性化的引导页面。
- 插件化开发： 易航网址引导系统支持插件化开发，目前内置了七款实用插件，用户可以根据需要自行开发定制插件，进一步增强系统的功能和灵活性。
- 轻量级且易于部署： 无需复杂的安装和解压过程，即可快速部署和使用。这大大降低了使用门槛，使得系统在不同的环境中都能轻松应用。
- 数据管理和安全性： 采用易航原创的JsonDb数据包进行数据管理，同时内置硬防洪和硬防墙插件，有效防范恶意攻击，确保系统的稳定和安全性。
- SEO友好： 对前台URL进行伪静态重写，这使得系统在搜索引擎中的表现更为友好，有助于提升页面的搜索排名和曝光度。

项目作者：易航

QQ：2136118039

- [官方网站](http://guide.bri6.cn)
- [Demo / 在线演示](http://guide.demo.bri6.cn/admin)
- [帮助文档](https://yepydvmpxo.k.topthink.com/@guide/)

## 系统亮点

1. 采用 [光年全新v5模板](https://gitee.com/yinqi/Light-Year-Admin-Template-v5) 开发后台
2. 后台内置8款主题色，分别是简约白、炫光绿、渐变紫、活力橙、少女粉、少女紫、科幻蓝、护眼黑
3. 可管理无数引导页主题并且主题内可以进行不同的自定义设置，目前内置二十多套主题 持续增加中...
4. 可单独开发各种插件，插件可进行自定义设置，目前内置七款实用插件
5. 无需安装解压部署即可使用
6. 数据管理包采用易航原创 [JsonDb](https://gitee.com/yh_IT/json-db) 数据包
7. 主题内置神奇的 `$this` 语法，可快速开发并保留著作版权
8. 对前台URL进行伪静态重写 对搜索引擎更加友好
9. 内置硬防洪和硬防墙插件 告别域名忧虑

## 安装教程

PHP版本：8.1、8.2

Nginx服务器首先站点部署伪静态

```nginx
location ~* ^/content/JsonDb/.*.(json)$ {
    deny all;
}
location / {
    if (!-e $request_filename){
        rewrite  ^(.*)$  /index.php?s=$1  last;   break;
    }
}
```

后台地址：域名/admin

初始账号：admin

初始密码：123456

因为本项目采用 [JsonDb](https://gitee.com/yh_IT/json-db) 数据包，所以免配置 `MySQL` 即可使用

[更新日志](/UPDATE.md)
